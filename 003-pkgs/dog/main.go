package main

import (
	"fmt"

	"gitlab.com/go-test/003-pkgs/cat"
)

func main() {
	fmt.Println("Hello from dog")
	cat.Hello()
	cat.Eat()
}
