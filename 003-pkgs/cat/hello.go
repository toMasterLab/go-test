package cat

import "fmt"

func Hello() {
	fmt.Println("Hello from cat")
}

func Eat() {
	fmt.Println("The cats eat birds")
}
