package main

import "fmt"

type person struct {
	nombre string
}

func (p *person) talk() {
	fmt.Println("Hello, I am", p.nombre)
}

type human interface {
	talk()
}

func saySomething(h human) {
	h.talk()
}

func main() {
	p1 := person{
		nombre: "tomy",
	}
	p1.talk()
	//saySomething(&p1)
}
