package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	fmt.Printf("CPU: %v\n", runtime.NumCPU())
	fmt.Printf("GoRutines: %v\n", runtime.NumGoroutine())

	increment := 0
	gs := 100
	wg.Add(gs)
	var m sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			m.Lock()
			v := increment
			v++
			increment = v
			fmt.Println(increment)
			m.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println("The final value of increment is ", increment)
}
