package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("CPU: %v\n", runtime.NumCPU())
	fmt.Printf("GoRutines: %v\n", runtime.NumGoroutine())

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		fmt.Println("Hello from the first routine")
		wg.Done()
	}()

	go func() {
		fmt.Println("Hello from the second routine")
		wg.Done()
	}()

	fmt.Printf("CPU: %v\n", runtime.NumCPU())
	fmt.Printf("GoRutines: %v\n", runtime.NumGoroutine())

	wg.Wait()
	fmt.Println("I have finished main")

	fmt.Printf("CPU: %v\n", runtime.NumCPU())
	fmt.Printf("GoRutines: %v\n", runtime.NumGoroutine())
}
