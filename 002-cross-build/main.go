package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("OS: %v/nArch: %v/n", runtime.GOOS, runtime.GOARCH)
}
